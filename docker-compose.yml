---
version: "3.8"

services:
  traefik:
    image: traefik:${TRAEFIK_VERSION:?err}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_traefik"
    restart: unless-stopped
    depends_on:
      - docker-socket-proxy
    networks:
      - web
      - docker-socket-proxy
      - backend
    ports:
      - "${HOST_IP:-0.0.0.0}:80:80"
      - "${HOST_IP:-0.0.0.0}:443:443"
      - "127.0.0.1:${TRAEFIK_API_PORT:-8079}:8080"
    volumes:
      - "./letsencrypt:/letsencrypt"
    command:
      - "--global.sendAnonymousUsage=false"
      - "--log.level=${TRAEFIK_LOG_LEVEL:-ERROR}"
      - "--api=${TRAEFIK_API_ENABLED:-false}"
      - "--api.insecure=true"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.web.http.redirections.entrypoint.to=websecure"
      - "--entrypoints.web.http.redirections.entrypoint.scheme=https"
      - "--entrypoints.web.http.redirections.entrypoint.permanent=true"
      - "--entrypoints.websecure.address=:443"
      - "--certificatesresolvers.route53.acme.dnschallenge=true"
      - "--certificatesresolvers.route53.acme.dnschallenge.provider=route53"
      - "--certificatesresolvers.route53.acme.storage=/letsencrypt/route53.json"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.endpoint=tcp://docker-socket-proxy:2375"
      - "--providers.docker.network=docker-socket-proxy"
    env_file:
      - .env

  docker-socket-proxy:
    image: tecnativa/docker-socket-proxy:${DOCKERPROXY_VERSION:-latest}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_docker-socket-proxy"
    restart: unless-stopped
    networks:
      - docker-socket-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro,delegated
    environment:
      CONTAINERS: 1

  alertmanager:
    image: prom/alertmanager:v0.21.0
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_alertmanager"
    restart: unless-stopped
    networks:
      - backend
    ports:
      - 9093:9093
    command: --config.file=/etc/alertmanager/alertmanager.yml
    volumes:
      - ./alertmanager/alertmanager.yml:/etc/alertmanager/alertmanager.yml
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.alertmanager.entrypoints=websecure"
      - "traefik.http.routers.alertmanager.rule=${ALERTMANAGER_FQDN:?err}"
      - "traefik.http.routers.alertmanager.tls.certresolver=route53"
      - "traefik.http.routers.alertmanager.tls=true"
      - "traefik.http.routers.alertmanager.middlewares=basic-auth@docker"
      - "traefik.http.middlewares.basic-auth.basicauth.users=${TRAEFIK_BASIC_AUTH}"

  prometheus:
    image: prom/prometheus:v2.22.2
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_prometheus"
    restart: unless-stopped
    networks:
      - backend
    ports:
      - 9090:9090
    command:
      - "--config.file=/etc/prometheus/prometheus.yml"
      - "--web.external-url=${PROMETHEUS_EXTERNAL_URL}"
    volumes:
      - prometheus_data:/prometheus
      - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - ./prometheus/targets:/etc/prometheus/targets
      - ./prometheus/rules:/etc/prometheus/rules
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.prometheus.entrypoints=websecure"
      - "traefik.http.routers.prometheus.rule=${PROMETHEUS_FQDN:?err}"
      - "traefik.http.routers.prometheus.tls.certresolver=route53"
      - "traefik.http.routers.prometheus.tls=true"
      - "traefik.http.routers.prometheus.middlewares=basic-auth@docker"
      - "traefik.http.middlewares.basic-auth.basicauth.users=${TRAEFIK_BASIC_AUTH}"

  blackbox:
    image: prom/blackbox-exporter:v0.18.0
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_blackbox"
    restart: unless-stopped
    command: --config.file=/config/blackbox.yml
    volumes:
      - ./blackbox/blackbox.yml:/config/blackbox.yml
    networks:
      - backend
    ports:
      - 9115:9115

networks:
  web:
    internal: false
  docker-socket-proxy:
    internal: true
  backend:
    internal: false
...
